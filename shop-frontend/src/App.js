import React, { Component } from "react";
import "./App.css";
//import ShopContent from './components/ShopContent';
import { MYCARTOKEN } from "./components/Environment";
import { BrowserRouter as Router, Route } from "react-router-dom";
import ShopContent from "./components/ShopContent";
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import { AuthProvider } from "./components/Auth";
import PrivateRoute from "./components/PrivateRoute";
import BuyCart from "./components/BuyCart";

class App extends Component {
  constructor() {
    super();
    sessionStorage.removeItem(MYCARTOKEN);
  }
  render() {
    return (
      <AuthProvider>
        <Router>
          <div>
            <Route exact path="/" component={ShopContent} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/signup" component={SignUp} />
            <Route exact path="/buy" component={BuyCart} />
          </div>
        </Router>
      </AuthProvider>
    );
  }
}

export default App;
