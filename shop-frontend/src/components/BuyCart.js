import React, { Component } from "react";
import CartComponent from "./CartComponent";
import { HOSTURL, MYCARTOKEN, SHOPURL } from "./Environment";
import app from "./base";

// https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
class ShopContent extends Component {
  constructor() {
    super();
    this.state = { articles: [], cart: {} };
  }

  componentDidMount() {
    fetch(SHOPURL + "articles/")
      .then((response) => response.json())
      .then((articles) => this.setState({ articles }));
  }

  addArticleToCart = (articleId) => {
    let token = JSON.parse(sessionStorage.getItem(MYCARTOKEN));
    if (token === null) {
      fetch(SHOPURL + "carts", {
        method: "POST",
        headers: { "Content-Type": "application/json; charset=utf-8" },
      })
        .then((response) => response.json())
        .then((token) => {
          sessionStorage.setItem(MYCARTOKEN, JSON.stringify(token));
          this.addArticleToCartSub(articleId, token.id);
        });
    } else {
      this.addArticleToCartSub(articleId, token.id);
    }
  };
  addArticleToCartSub = (id, tokenId) => {
    fetch(SHOPURL + "carts/" + tokenId, {
      method: "POST",
      headers: { "Content-Type": "application/json; charset=utf-8" },
      body: JSON.stringify({ id }),
    })
      .then((response) => response.json())
      .then((cart) => {
        this.setState({ cart });
      });
  };

  deleteArticleFromCart = (id) => {
    let token = JSON.parse(sessionStorage.getItem(MYCARTOKEN));
    fetch(SHOPURL + "carts/" + token.id, {
      method: "DELETE",
      headers: { "Content-Type": "application/json; charset=utf-8" },
      body: JSON.stringify({ id }),
    })
      .then((response) => response.json())
      .then((cart) => {
        this.setState({ cart });
      });
  };

  render() {
    var user = app.auth().currentUser;
    var email;
    if (user != null) {
      email = "Signed in as " + user.email;
    }
    let articles = this.state.articles.map((a) => (
      <div className="card" key={a.id}>
        <img className="card-img-top" src={HOSTURL + a.image} alt="" />
        <div className="card-body">
          <h5 className="card-title">{a.text}</h5>
          <p className="card-text">{a.description}</p>
          <button
            onClick={() => this.addArticleToCart(a.id)}
            className="btn btn-primary"
          >
            add to cart
          </button>
        </div>
      </div>
    ));
    let token = sessionStorage.getItem(MYCARTOKEN);
    return (
      <div className="container">
        <div clasee="title" className="row">
          <div className="col-md-9">
            <h1>Beverage shop</h1>
          </div>
          <div className="col align-self-end">{email}</div>
          <div className="col align-self-end">
            <button
              className="btn btn-primary"
              onClick={() => app.auth().signOut()}
            >
              Sign out
            </button>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-5">
            <CartComponent
              cartToken={token}
              cart={this.state.cart}
              addToCart={this.addArticleToCart}
              deleteFromCart={this.deleteArticleFromCart}
            />
            <button
              className="btn btn-primary"
              onClick={() => (window.location.href = "/login")}
            >
              Bezahlen
            </button>
          </div>
        </div>
      </div>
    );
  }
}
export default ShopContent;
