import React, { Component } from "react";

class CartContent extends Component {
  constructor() {
    super();
    this.state = { cartEntries: {} };
  }

  componentDidMount() {
    const cartToken = this.props.cartToken;
    if (cartToken) {
      fetch("carts/" + cartToken)
        .then((response) => response.json())
        .then((cart) => {
          console.log("Cart " + cart);
          console.log(cart.cartEntries);
        });
    }
  }

  render() {
    let list = [];
    if (this.props.cart.cartEntries) {
      this.props.cart.cartEntries.forEach((ce) => console.log(ce));
      list = this.props.cart.cartEntries.map((ce, key) => (
        <div key={key}>
          <span key={ce.article.id}>
            {ce.count} {ce.article.text} {ce.article.price}/{ce.preis}{" "}
          </span>
          <button
            onClick={() => this.props.addToCart(ce.article.id)}
            className="btn btn-primary btn-circle btn-sm"
          >
            +
          </button>
          &nbsp;
          <button
            onClick={() => this.props.deleteFromCart(ce.article.id)}
            className="btn btn-sm btn-primary"
          >
            -
          </button>
        </div>
      ));
    }
    return (
      <div>
        <h2>
          <img src="https://img.icons8.com/metro/26/000000/shopping-cart.png" />
        </h2>
        {list}
        {!this.props.cart.count && <p>cart is empty</p>}
        {this.props.cart.count && (
          <p>cart contains {this.props.cart.count} entries</p>
        )}
      </div>
    );
  }
}
export default CartContent;
