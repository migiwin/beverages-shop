import React, { useCallback, useContext } from "react";
import { withRouter, Redirect } from "react-router";
import app from "./base.js";
import { AuthContext } from "./Auth.js";

const Login = ({ history }) => {
  const handleLogin = useCallback(
    async (event) => {
      event.preventDefault();
      const { email, password } = event.target.elements;
      try {
        await app
          .auth()
          .signInWithEmailAndPassword(email.value, password.value);
        history.push("/");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/" />;
  }

  return (
    <div class="loginContent">
      <h1>Log in</h1>

      <form onSubmit={handleLogin}>
        <div class="form-group">
          <label for="exampleInputEmail1">
            Email
            <input
              type="email"
              class="form-control"
              aria-describedby="emailHelp"
              placeholder="Enter email"
            />
          </label>
        </div>

        <div class="form-group">
          <label>
            Password
            <input
              type="password"
              class="form-control"
              placeholder="Password"
            />
          </label>
        </div>
        <button type="submit" className="btn btn-primary">
          Log in
        </button>
      </form>
      <div class="signup">
        <button
          className="btn btn-outline-secondary"
          onClick={() => (window.location.href = "/SignUp")}
        >
          Registrieren
        </button>
      </div>
    </div>
  );
};

export default withRouter(Login);
