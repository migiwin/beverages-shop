import React, { useCallback } from "react";
import { withRouter } from "react-router";
import app from "./base";

const SignUp = ({ history }) => {
  const handleSignUp = useCallback(
    async (event) => {
      event.preventDefault();
      const { email, password } = event.target.elements;
      try {
        await app
          .auth()
          .createUserWithEmailAndPassword(email.value, password.value);
        history.push("/");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  return (
    <div class="signUpContent">
      <h1>Registrieren</h1>
      <form onSubmit={handleSignUp}>
        <div class="form-group">
          <label for="exampleInputEmail1">
            Email
            <input
              name="email"
              type="email"
              class="form-control"
              aria-describedby="emailHelp"
              placeholder="Enter email"
            />
          </label>
        </div>

        <div class="form-group">
          <label>
            Password
            <input
              name="password"
              type="password"
              class="form-control"
              placeholder="Password"
            />
          </label>
        </div>
        <button type="submit" className="btn btn-primary">
          Registrieren
        </button>
      </form>
    </div>
  );
};

export default withRouter(SignUp);
