# <b> Sicherheitskonzept </b>

## E-Business "Getränke Online Shop"

Wir wollen einen Getränke Online Shop erstellen mit dem man Online Getränke-Bestellungen tätigen kann. Der Shop soll so sicher wie möglich sein. Um Sicherheitsrisiken zu vermeiden, wollen wir hauptsächlich verhindern, dass Personen vorsätzlich, oder aber auch unabsichtlich, Schaden an der App verursachen. Um diese Risiken zu verhindern wollen wir schon bei der Plannung so viel wie möglich beachten und Vorkehrungen treffen.

## Was wollen wir konkret Umsetzten

Wir werden versuchen eine einfachere Applikation umzusetzen in der man Getränke in den Warenkorb packen kann. Das Zahlungssystem werden wir höchst warscheindlich nur theoretisch umsetzten. Man sollte sich auf der Seite Einloggen können.

# Gefahren

## Unbefugter Datenzugriff

Bei unserem Getränke-Shop ist ganz wichtig, dass die Gefahr des Unbefugten Datenzugriffs immer bedacht wird. Der Shop verarbeitet vertrauliche Informationen des Kunden. Diese Informationen dürfen nicht für unbefugte Personen zugänglich sein.

## Unerlaubte Datenmanipulation

Die Kundendaten dürfen ebenfalls nicht von unbefugten Personen veränderbar sein.

## Verlust der Verfügbarkeit

Daten des Shops und Kundendaten müssen sicher aufbewahrt werden, sodass sie im Notfall für befugte Personen immer Zugänglich sind.

# Schutzziele

## Vertraulichkeit

Vertrauliche Daten müssen vor unbefugtem Zugriff geschützt werden.

## Verfügbarkeit

Der Getränke-Shop und seine Daten müssen dem Kunden jederzeit zur verfügung Stehen.

## Integrität

Die Daten dürfen nicht nachträglich verändert werden, sondern sollen immer echt bleiben. Die gespeicherten Daten müssen immer der orginalen Eingabe des Kunden entsprechen.

## Authentizität

Die Identität jedes Kunden muss zweifelsfrei belegt werden können und darf nachträglich nicht geändert werden dürfen.

## Verbindlichkeit

Transaktion zwischen Kunden und Shop müssen immer verbindlich sein.

# IT-Sicherheit

Bei der IT-Sicherheit soll der Shop vor allem die sichere Verwahrung und Verarbeitung von vertraulichen Kunden-, Mitarbeiter-, Finanz- und Lieferantendaten sicherstellen und gewährleisten. Zudem soll der Shop auch den Vorkehrungen des DSG, der VDSG, des Urheberrechtsgesetz und dem Strafgesetzbuch nachgehen.

Die Sicherheitspolitik und die Sicherheitsanweisungen sollen nach den oben genannten Kriterien strukturiert sein.

# Umgesetzer Teil

Kurz gesagt konnten wir beim einen einfache Shop programmieren, bei dem man Getränke in den Warenkorb legen kann. Zudem kann man einen Account erstellen, indem man auf Bezahlen klickt. Denn sobald ein Kunde seine Ware bezahlen will, muss er einen Account erstellen. Um sich zu registrieren, muss der Kunde seine email und sein Passwort eingeben.

## Athentifizierung

Wir haben bei der Authenifierung mit [Firebase](https://firebase.google.com/)
gearbeitet. Dabei mussten wir ein Firebase Projekt erstellen und die Authentifierung für dieses einrichten.  
Sobal ein User auf Bezahlen klickt, muss er sich einloggen, falls er noch keinen Account hat kann er sich registrieren.

Folgende Punkte haben wir dabei beachet:

### XSS

Durch Bereinigung der Eingabefelder kann man vor XSS-Angriffen schützen.

### Injection

Durch Sanitierung der Eingabefelder kann man vor Injection schützen.

### Sensitive Data Exposure

Durch aktuelle Verschlüsselung und Hashing können Daten sicherer gelagert werden.

### Broken Authentication & Broken Access Control

Indem man den Benutzer dazu auffordert ein sichereres Passwort zu verwenden kann man Risiken vermeiden. Auch indem man Benutzernamen nur einmalig verwenden kann, oder das Email-Feld ein "@" beinhalten muss, kann man Risiken der Datengefährdung vermeiden.
![alt text](./images/1.png)
![alt text](./images/2.png)

## Firebase

Die Authentifiezierung mit Firebase [Authentifiezierung mit Firebase](https://firebase.google.com/docs/auth) zu machen ist einfach und sicher. Firebase übernimmt für unsere App die Authenfizierung und ist dafür verantwortlich, dass die Daten der Kunden sicher abgespeichert werden.
![alt text](./images/3.png)

# Zeitschätzung für nicht umgesetzte Softwarekomponente

## Backup

Mit einem Backup der Daten auf einem anderen Server könnten Datenverlustrisiken verringert werden.
Zeitschätzung: ca 2h

## Tansaktion

Um Getränke im Shop zu kaufen, müssten die Kunde natürlich auch bezahlen. Um dies in unsere App einzusentzen müssten wir auf eine Api zurück greifen. Dabei haben wir an die [Stripe Api](https://stripe.com/docs/api)
gedacht. Wir müssten dabei natürlich auch eine neue Seite einfügen.  
Die Transaktionssicherheit gewährleistet, dass in einer Datenbank durchgeführte Aktionen wie das Ändern oder das Anlegen von Datensätzen einen fehlerfreien und in sich konsistenten Datenbestand hinterlassen. Im Rahmen der Transaktionssicherheit werden einzelne Transaktionen entweder vollständig oder gar nicht ausgeführt. Findet eine fehlerhafte Transaktion statt, ist diese abzubrechen und der vorherige Zustand der Datenbank wiederherzustellen.
Zeitschätzung: ca 3h.

## Abspreichern von Daten

Um den Shop effektiv zu mache, müssten wir die Dataen der Kunden abspeichern und darauf achten dass alle Kunden zu ihren Waren kommen. Wir müssten dabei z.B wissen welches Kunde welche Getränke bestellt hat. Dies müssten wir in einer DB abspeichern.
Zeitschätzung: ca 3h.

## Private Route

Wir haben in unserem Projekt eine PrivateRoute.js Datei. Diese wäre dazu, um einen Seiten unzugägnlich zu machen, wenn man nicht eingeloggt ist oder halt nicht berechtigt ist. Z.B man kommt nur zur Transaktion-Seite, wenn man sich eingeloggt hat. Wir konnte dies noch nicht implementieren, da es bei unserem Shop noch keinen Sinn macht. Wir müssten dafür die z.B Transaktion imlementieren.
Zeitschätzung: ca 30min.

# Sicherheitslücken

Die Applikation sollte so viele Sicherheitslücken wie möglich abdecken. Dazu gehören Sicherheitslücken im Bereich Organisatorische Mängel, Menschliche Fehlhandlung, Vorsätzliche Handlung, Technisches Versagen und höhere Gewalt

Die Applikation sollte vor Sicherheitsproblemen wie Injection und XSS geschützt sein. Aber auch Dinge wie Broken Authentication, Broken Access Control und Sensitive Data Exposure vermeiden. Damit werden vor allem Menschlichen und Vorsätzlichen Handlungen vorgebeugt. Dazu kann man sich auch noch Gedanken über Backup Möglichkeiten der Daten machen, um bei technischem Versage oder höherer Gewalt vorbereitet zu sein.

# Reflexion Nicolas

Für unseren Getränke-Shop mussten wir uns einige Gedanken über die Sicherheit, sowie die Seite an sich machen. Dabei haben wir ein einiges über die Sicherheitsprobleme und die zu treffenden Schutzmassnahmen gelernt. Dazu konnten wir uns noch einige Gedanken zu der Implementierung von Schutzmassnahmen machen, um das ganze Thema weiter zu verinnerlichen. Schlussendlich haben wir doch einiges zum Thema IT-Sicherheit dazu gelernt.

# Reflexion Michael

Durch dieses Projekt habe ich gelernt, wie ich in Zunkuft richtig mit Daten umgehe, wenn ich selbst einmal eine Webseite erstelle, die mit Daten zu tun hat. Durch das erstellen des Shop habe ich gemerkt wie wichtig es ist von vorne rein gut geplant zu haben. Ich habe einiges über das Umgehen mit Daten und das treffen von geeigneten Schutzmassnahmen gelernt. Da wir einige Probleme mit React hatten kamen wir leider mit mit der Transaktion nicht so weit. Aber ich denke wir konnten trotzem vieles über die Sicherheit lernen.
