package ch.bbw.shop;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ch.bbw.shop.model.Article;
import ch.bbw.shop.model.Cart;

@RestController
@RequestMapping("/carts")
public class CartService {
	
	@Autowired
	private CartDAO cartDAO;
	
	@Autowired 
	private ArticleDAO articleDAO;
	
	@GetMapping
	public ResponseEntity<String> getCarts() {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PostMapping
	public Token createCart() {
		Token token = cartDAO.createCart();
		System.out.println("create cart : " + token.getId());
		return token;
	}

	@GetMapping("/{token}")
	public Cart getCart(@PathVariable("token")String tokenId) {
		System.out.println("getCart: " + tokenId);
		Optional<Cart> cart = cartDAO.getCardByTokenId(tokenId);
		if (cart.isPresent()) {
			return cart.get();
		} else {
			// TODO: return status 404
			return null;
		}
	}

	@PostMapping("/{token}")
	public Cart addArticleToCart(@PathVariable("token")String tokenId, @RequestBody ArticleId articleId) {
		Optional<Cart> cart = cartDAO.getCardByTokenId(tokenId);
		if (cart.isPresent()) {
			Optional<Article> article = articleDAO.getArticleById(articleId.id);
			if (article.isPresent()) {
				cart.get().addArticle(article.get());
				return cart.get();
			} 
		} 
		// TODO: return status 404
		return null;

	}


	@DeleteMapping("/{token}")
	public Cart deleteArticleFromCart(@PathVariable("token")String tokenId, @RequestBody ArticleId articleId) {
		Optional<Cart> cart = cartDAO.getCardByTokenId(tokenId);
		if (cart.isPresent()) {
			System.out.println("Warenkorb gefunden");
			Optional<Article> article = articleDAO.getArticleById(articleId.id);
			if (article.isPresent()) {
				System.out.println("Artikel gefunden");
				cart.get().delArticle(article.get());
				return cart.get();
			}
		}
		// TODO: return status 404
		return null;

	}
}

class ArticleId { public int id; }
