package ch.bbw.shop;

public class Token {
	
	private String id;

	public Token(String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
