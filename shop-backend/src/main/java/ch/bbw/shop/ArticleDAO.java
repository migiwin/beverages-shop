package ch.bbw.shop;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import ch.bbw.shop.model.Article;

@Service
public class ArticleDAO {
	
	private List<Article> articles;
	
	public ArticleDAO() {
		articles = new ArrayList<>();
		articles.add(new Article(0, "0001", "Red Bull Energy Drink 355ml 24-Pack", "Hält wach und gibt Energie", 39.90));
		articles.add(new Article(1, "0002", "Red Bull Energy Drink 250ml 24-Pack", "Hält wach und gibt Energie", 50.00));
		articles.add(new Article(2, "0003", "Coca-Cola 6 x 0.33 l", "Ohne zugesetzte Konservierungsstoffe", 4.20));
		articles.add(new Article(3, "0004", "Coca-Cola Zero 6 x 0.33 l", "Echter Geschmack und zero Zucker", 4.20));
		articles.add(new Article(4, "0005", "Eistee Lemon 6 x 0.33 l", "Bester Lipton Ice Tea mit einem Schuss fruchtig-spritziger Zitrone", 4.70));
		articles.add(new Article(5, "0006", "Rivella Rot 6 x 0,5 l", "Hergestellt nach dem geheimen Originalrezept", 8.40));
		articles.add(new Article(6, "0007", "Premium Mixer Tonic & Hibiscus 4 x 0.2 l", "Fruchtige Aromen der Hibiskusblüte", 7.50));
		articles.add(new Article(7, "0008", "Soft-Drinks Schorle 6 x 0.5 l", "Schluck für Schluck erfrischender Genuss", 9.40));

		articles.get(0).setImage("images/0001.jpg");
		articles.get(1).setImage("images/0002.jpg");
		articles.get(2).setImage("images/0003.jpg");
		articles.get(3).setImage("images/0004.jpg");
		articles.get(4).setImage("images/0005.jpg");
		articles.get(5).setImage("images/0006.jpg");
		articles.get(6).setImage("images/0007.jpg");
		articles.get(7).setImage("images/0008.jpg");
	}
	
	public List<Article> getArticles() {
		return articles;
	}

	public Optional<Article> getArticleById(int id) {
		return articles.stream()
			.filter(a -> a.getId()==id)
			.findFirst();
	}

}
