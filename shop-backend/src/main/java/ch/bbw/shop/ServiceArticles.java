package ch.bbw.shop;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.bbw.shop.model.Article;

@RestController
@RequestMapping("/articles")
public class ServiceArticles {

	@Autowired
	private ArticleDAO articleDAO;
	
	@GetMapping("/greeting")
    public Greeting greeting() {
        return new Greeting(2, "Peter");
    }
	
	@GetMapping("/")
	public List<Article> getArticles() {
		return articleDAO.getArticles();
	}
	
	@GetMapping("/{id}")
	public Article getArticle(@PathVariable("id")int id) {
		Optional<Article> article = articleDAO.getArticleById(id);
		if (article.isPresent()) {
			return article.get();
		} else {
			return null;			
		}
	}
	
}

