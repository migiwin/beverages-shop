package ch.bbw.shop;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

import ch.bbw.shop.model.Cart;

@Service
public class CartDAO {
	
	private HashMap<Token, Cart> carts;
	
	public CartDAO() {
		carts = new HashMap<>();
	}
	
	public Token createCart() {
		Cart cart = new Cart();
		
		Token token = new Token(UUID.randomUUID().toString());
		carts.put(token, cart);
		return token;
	}

	public Optional<Cart> getCardByTokenId(String tokenId) {
		Optional<Token> t = carts.keySet().stream()
				.filter(k -> k.getId().equals(tokenId))
				.findFirst();
		if (t.isPresent()) {
			return Optional.of(carts.get(t.get()));
		} else {
			System.out.println("getCartByTokenId: not found");
			return Optional.empty();
		}
	}
	
	public Collection<Token> getAllCartTokens() {
		return carts.keySet();
	}

}
